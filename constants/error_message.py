ERROR_MESSAGE_INVALID_MATH_OPERATION = """\
Maaf, operasi yang Anda masukan tidak valid.
Silakan cek kembali operasi matematika yang Anda masukan.

Beberapa hal yang dapat Anda cek:
(1) Pastikan Anda memberikan jarak (spasi) di antara operator dengan angka, seperti contoh berikut: 1 x 1;
(2) Pastikan tidak ada operator yang bersebelahan dengan operator lainnya, seperti contoh berikut: 1 x x 1 atau 1 xx 1;
(3) Pastikan tidak ada angka yang terpisahkan oleh spasi, seperti contoh berikut: 1 1 x 1.

Jika Anda sudah yakin, silakan Anda coba lagi.\
"""

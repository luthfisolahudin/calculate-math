MATH_OPERATORS = {
    "FIRST_PRIORITY": {"x": lambda first_number, second_number: first_number * second_number,
                       "/": lambda first_number, second_number: first_number / second_number,
                       "*": lambda first_number, second_number: first_number * second_number,
                       ":": lambda first_number, second_number: first_number / second_number},
    "SECOND_PRIORITY": {"+": lambda first_number, second_number: first_number + second_number,
                        "-": lambda first_number, second_number: first_number - second_number}
}
